*** Settings ***
Library    SeleniumLibrary
Library    OperatingSystem
Library    String
Library    Collections
Library    DateTime

*** Variables ***

*** Keywords ***
Open Local Browser
    Test Browser Setup      Chrome

Test Browser Setup
    [Arguments]  ${BROWSER}
    Run Keyword If  '${BROWSER}'=="Chrome"    Create Webdriver   ${BROWSER}   executable_path=C:/Users/CCASTRO/PycharmProjects/iScale/Utility/Drivers/chromedriver.exe
    Go To  https://qrqck8u5t9co.front.staging.optimy.net/en/
    Maximize Browser Window

Test Close Browser
    Close All Browsers

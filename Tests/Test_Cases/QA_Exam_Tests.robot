*** Settings ***
Library    SeleniumLibrary
Library    OperatingSystem
Library    String
Library    Collections
Library    DateTime
Resource  Utility/Commons.robot
Resource  Resources/StepsDefinition/QA_Exam_SD.robot

Test Setup      Open Local Browser
Test Teardown   Test Close Browser

*** Test Cases ***
Submit New Application
    [Tags]  RUN
    [Documentation]  Verify that the user should be able to submit new application.
    Given I am on the dashboard page
    When I close the cookie modal
    And I click the submit application button in dashboard
    Then I am on Login Page
    When I enter optimyautomationtester@gmail.com valid email address
    And I enter 2MsWseoj valid password
    And I click login
    Then Verify if the "Continue with the submission of an application" is present
    When I enter my First Name is Kathryn
    And My Last Name is Bernardo
    And My Address is in Valenzuela City
    And postal code is 1000
    And My Country is Philippines
    And I Upload My Photo
    And My gender is Female
    And I am applying for Automation tester
    And I am familiar with JIRA, Java, Python, Robot Framework
    And My Career objective is QA Engineer with over 5 years of overallexperience as QA with 3 years solidexperience in Automation. I can easilyadapt to new platforms, environments,frameworks, and programminglanguages. My strength is businessawareness. It enables me to streamlineflows, methods, and logic of applications.
    And I click Next Screen
    Then First Name is Displayed
    And Last Name is Displayed
    And Address is Displayed
    And Gender is Displayed
    And Postal Code is displayed
    And Tools are displayed
    And Career Objective is displayed
    When I Click Validate and Send
    Then Successfully Redirected on Success Dashboard

*** Settings ***
Library    SeleniumLibrary
Library    OperatingSystem
Library    String
Library    Collections
Library    DateTime
Resource   Resources/Pages/QA_Exam_Page.robot
*** Variables ***

*** Keywords ***
I am on the dashboard page
    Get The Title of Browser

I click the submit application button in dashboard
    Submit Application Button

I close the cookie modal
    Close Modal

I am on Login Page
    Check if Login Page

I enter ${test_data} valid email address
    Input Valid Email   ${test_data}

I enter ${test_data} valid password
    Input Valid Password    ${test_data}

I click login
    Click Login Button

Verify if the "Continue with the submission of an application" is present
    Check if the continue with submission of an application if present

I enter my First Name is ${fname}
    Input First Name    ${fname}

My Last Name is ${lname}
    Input Last Name     ${lname}

My Address is in ${add}
    Input Address   ${add}

Postal Code is ${postal}
    Input Postal Code   ${postal}

My Country is ${test_data}
    Select Country  ${test_data}

I Upload My Photo
    Upload Photo

My gender is ${test_data}
    Select Gender   ${test_data}

I am applying for ${test_data}
    Select Jobs     ${test_data}

I am familiar with ${jira}, ${java}, ${python}, ${rf}
    Select Automation Tools     ${jira}
    Select Automation Tools     ${java}
    Select Automation Tools     ${python}
    Select Automation Tools     ${rf}

My Career objective is ${test_data}
    Input Career Objective      ${test_data}

I click Next Screen
    Click Next Screen Button

First Name is Displayed
    Verify if First Name is displayed

Last Name is Displayed
    Verify if Last Name is displayed

Address is Displayed
    Verify if Address is displayed

Gender is Displayed
    Verify if Gender is displayed

Postal Code is displayed
    Verify if Postal Code is displayed

Tools are displayed
    Verify if Jira Tool is displayed
    Verify if Java Tool is displayed
    Verify if Python Tool is displayed
    Verify if RF Tool is displayed

Career Objective is displayed
    Verify if Career Objective is displayed

I Click Validate and Send
    Click Validate and Send

Successfully Redirected on Success Dashboard
    Succes Message

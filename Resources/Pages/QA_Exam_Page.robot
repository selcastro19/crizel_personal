*** Settings ***
Library    SeleniumLibrary
Library    OperatingSystem
Library    String
Library    Collections
Library    DateTime

*** Variables ***
${BTN_SUBMIT_APPLICATION}     css=a[href="project/new/"]
${BTN_CLOSE_COOKIE_MODAL}     css=button#cookie-close
${TXT_EMAIL_FIELD}      css=input#login-email
${TXT_PASSWORD_FIELD}      css=input#login-password
${LBL_LOGIN_DASH}      css=h1.h1
${BTN_LOGIN}      css=button.btn-primary:nth-child(2)
${BTN_SUBMIT_NEW_APPLICATION}      css=a.btn-outline-primary
${LBL_CONTINUE_WITH_APPLICATION}      css=h1.h1
${TXT_FIRSTNAME}      css=input[aria-label="First name"]
${TXT_LASTNAME}      css=input[aria-label="Last name"]
${TXT_ADDRESS}      css=textarea[aria-label="Unit no/House no, Street"]
${TXT_POSTAL_CODE}      css=input.ui-autocomplete-input
${DRP_COUNTRY}      css=select[aria-label="Country"]
${DRP_JOBS}      css=select[aria-label="Select a role you're applying for"]
${TXT_CAREER}      css=body.cke_editable[tabindex="0"]
${BTN_NEXT_SCREEN}      css=button#navButtonNext
${UPLOAD_FILE}      css=input[type="file"]
${LBL_FIRSTNAME}      css=div#container_06c8a27e-7d11-57b2-9286-af8fc8ba5b27
${LBL_LASTNAME}      css=div#container_9d848df4-cdd0-50aa-820f-fdedcbda7e11
${LBL_ADDRESS}      css=div#container_17540589-1aa5-5bf7-93fa-d49acf58b711
${LBL_POSTAL}      css=div#container_5911b832-9522-524a-9f3c-8014c2ddba1c
${LBL_COUNTRY}      css=div#container_bf6f7c7f-1da5-55d7-99ac-2866e4a139fd
${LBL_GENDER}      css=li#container_7950ee95-cb9c-554a-b6e6-8109544899f9
${LBL_JIRA}      css=li#container_60e2a93c-78a8-5f8f-ad08-fda1321f910d
${LBL_JAVA}      css=li#container_f48db7ae-4810-5db3-a4fa-bebe82bd20b8
${LBL_PYTHON}      css=li#container_e6456bec-eb42-5d46-9cc5-af39cf9028a8
${LBL_RF}      css=li#container_168fbd0e-3b12-5567-bb36-6959c521ffae
${LBL_CAREER}      css=p[class="mb-0 field"]
${BTN_VALIDATE}      css=button[class="btn btn-primary ml-md-auto"]
${LBL_SUCCESS}      css=h1.h1

${fname}
${lname}
${add}
${postal}
${country}
${gender}
${tools}
${jira}
${java}
${python}
${rf}
${career}

*** Keywords ***
Get The Title of Browser
    ${get_title}    Get Title
    Should Be Equal  ${get_title}       QA exam instance

Close Modal
    Wait Until Keyword Succeeds     10     2000ms      Click Element   ${BTN_CLOSE_COOKIE_MODAL}

Submit Application Button
    Wait Until Keyword Succeeds     10     2000ms      Click Element  ${BTN_SUBMIT_APPLICATION}

Check if Login Page
    Wait Until Keyword Succeeds     10     2000ms      Element Should Be Visible   ${LBL_LOGIN_DASH}

Input Valid Email
    [Arguments]  ${test_data}
    Wait Until Keyword Succeeds  10         2000ms      Input Text      ${TXT_EMAIL_FIELD}      ${test_data}

Input Valid Password
    [Arguments]  ${test_data}
    Wait Until Keyword Succeeds  10         2000ms      Input Text      ${TXT_PASSWORD_FIELD}      ${test_data}

Click Login Button
    Wait Until Keyword Succeeds  10         2000ms      Click Element    ${BTN_LOGIN}

Click Submit New Application
    Wait Until Keyword Succeeds  10         2000ms      Click Element    ${BTN_SUBMIT_NEW_APPLICATION}

Check if the continue with submission of an application if present
    ${isPresent}    Run Keyword And Return Status  Element Should Be Visible    ${LBL_CONTINUE_WITH_APPLICATION}
    Run Keyword If  "${isPresent}"=="True"      Click Element       ${BTN_SUBMIT_NEW_APPLICATION}

Input First Name
    [Arguments]  ${test_data}
    Wait Until Keyword Succeeds  10         2000ms      Input Text      ${TXT_FIRSTNAME}    ${test_data}
    ${fname}    Set Global Variable      ${test_data}

Input Last Name
    [Arguments]  ${test_data}
    Wait Until Keyword Succeeds  10         2000ms      Input Text      ${TXT_LASTNAME}    ${test_data}
    ${lname}    Set Global Variable      ${test_data}

Input Address
    [Arguments]  ${test_data}
    Wait Until Keyword Succeeds  10         2000ms      Input Text      ${TXT_ADDRESS}    ${test_data}
    ${add}    Set Global Variable      ${test_data}

Input Postal Code
    [Arguments]  ${test_data}
    Wait Until Keyword Succeeds  10         2000ms      Input Text      ${TXT_POSTAL_CODE}      ${test_data}
    Sleep  2s
    Press Keys      ${TXT_POSTAL_CODE}       \ue015
    Press Keys      None      ENTER
    ${postal}    Set Global Variable      ${test_data}

Select Country
    [Arguments]   ${test_data}
    Wait Until Keyword Succeeds  10         2000ms      Select From List By Label   ${DRP_COUNTRY}      ${test_data}
    ${country}   Set Global Variable      ${test_data}

Upload Photo
    Wait Until Keyword Succeeds  10         2000ms      Scroll Element Into View    ${UPLOAD_FILE}
    Choose File     ${UPLOAD_FILE}          C:/Users/CCASTRO/PycharmProjects/iScale/Utility/Test_Data/test.jpg

Select Gender
    [Arguments]  ${test_data}
    Wait Until Keyword Succeeds  10         2000ms      Click Element       css=label[aria-label="${test_data}"]
    ${gender}   Set Global Variable      ${test_data}

Select Jobs
    [Arguments]  ${test_data}
    Wait Until Keyword Succeeds  10         2000ms      Select From List By Label   ${DRP_JOBS}     ${test_data}
    ${tools}   Set Global Variable      ${test_data}

Select Automation Tools
    [Arguments]  ${test_data}
    Wait Until Keyword Succeeds  10         2000ms      Click Element   css=label[aria-label="${test_data}"]
    ${jira}    Set Global Variable      ${test_data}
    ${java}    Set Global Variable      ${test_data}
    ${python}    Set Global Variable      ${test_data}
    ${rf}    Set Global Variable      ${test_data}

Input Career Objective
    [Arguments]  ${test_data}
    Wait Until Keyword Succeeds      10         2000ms       Select Frame     css=iframe.cke_reset
    Wait Until Keyword Succeeds      10         2000ms       Input Text   ${TXT_CAREER}    ${test_data}
    ${career}   Set Global Variable      ${test_data}
    Unselect Frame

Click Next Screen Button
    Wait Until Keyword Succeeds     10         2000ms      Click Element   ${BTN_NEXT_SCREEN}

Verify if First Name is displayed
    Wait Until Keyword Succeeds     10         2000ms       Element Should Be Visible   ${LBL_FIRSTNAME}
    ${get_text}     Get Text    ${LBL_FIRSTNAME}
    ${remove_space}     Remove String       ${get_text}     ${SPACE}
    ${isTrue}   Run Keyword And Return Status    Should Be Equal     ${remove_space}     ${fname}
    Run Keyword If    "${isTrue}"=="True"     Set Test Message  First Name ${remove_space} is displayed\n     append=yes
    ...     ELSE    Log To Console  ${isTrue}

Verify if Last Name is displayed
    Wait Until Keyword Succeeds     10         2000ms       Element Should Be Visible   ${LBL_LASTNAME}
    ${get_text}     Get Text    ${LBL_LASTNAME}
    ${remove_space}     Remove String       ${get_text}     ${SPACE}
    ${isTrue}   Run Keyword And Return Status    Should Be Equal     ${remove_space}     ${lname}
    Run Keyword If    "${isTrue}"=="True"     Set Test Message  Last Name ${remove_space} is displayed\n      append=yes
        ...     ELSE    Log To Console  ${isTrue}

Verify if Address is displayed
    Wait Until Keyword Succeeds     10         2000ms       Element Should Be Visible   ${LBL_ADDRESS}
    ${get_text}     Get Text    ${LBL_ADDRESS}
    ${remove_space}     Remove String       ${get_text}     ${SPACE}
    ${isTrue}   Run Keyword And Return Status    Should Be Equal     ${remove_space}     ${add}
    Run Keyword If    "${isTrue}"=="True"     Set Test Message  Address ${remove_space} is displayed\n        append=yes
    ...     ELSE    Log To Console  ${isTrue}

Verify if Postal Code is displayed
    Wait Until Keyword Succeeds     10         2000ms       Element Should Be Visible   ${LBL_POSTAL}
    ${get_text}     Get Text    ${LBL_POSTAL}
    ${remove_space}     Remove String       ${get_text}     ${SPACE}
    ${isTrue}   Run Keyword And Return Status    Should Contain Any     ${remove_space}     ${postal}
    Run Keyword If    "${isTrue}"=="True"     Set Test Message  Postal Code ${remove_space} is displayed\n        append=yes
    ...     ELSE    Log To Console  ${isTrue}

Verify if Country is displayed
    Wait Until Keyword Succeeds     10         2000ms       Element Should Be Visible   ${LBL_COUNTRY}
    ${get_text}     Get Text    ${LBL_COUNTRY}
    ${remove_space}     Remove String       ${get_text}     ${SPACE}
    ${isTrue}   Run Keyword And Return Status    Should Contain Any     ${remove_space}     ${country}
    Run Keyword If    "${isTrue}"=="True"     Set Test Message  Country ${remove_space} is displayed\n        append=yes
    ...     ELSE    Log To Console  ${isTrue}

Verify if Gender is displayed
    Wait Until Keyword Succeeds     10         2000ms       Element Should Be Visible   ${LBL_GENDER}
    ${get_text}     Get Text    ${LBL_GENDER}
    ${remove_space}     Remove String       ${get_text}     ${SPACE}
    ${isTrue}   Run Keyword And Return Status    Should Contain Any     ${remove_space}     ${gender}
    Run Keyword If    "${isTrue}"=="True"     Set Test Message  Gender ${remove_space} is displayed\n        append=yes
    ...     ELSE    Log To Console  ${isTrue}

Verify if Jira Tool is displayed
    Wait Until Keyword Succeeds     10         2000ms       Element Should Be Visible   ${LBL_JIRA}
    ${get_text}     Get Text    ${LBL_JIRA}
    Log To Console  ${get_text}
    ${remove_space}     Remove String       ${get_text}     ${SPACE}
    ${isTrue}   Run Keyword And Return Status    Should Contain Any     ${remove_space}     ${jira}
    Run Keyword If    "${isTrue}"=="True"     Set Test Message   ${remove_space} is displayed\n        append=yes
    ...     ELSE    Log To Console  ${isTrue}

Verify if Java Tool is displayed
    Wait Until Keyword Succeeds     10         2000ms       Element Should Be Visible   ${LBL_JAVA}
    ${get_text}     Get Text    ${LBL_JAVA}
    Log To Console  ${get_text}
    ${remove_space}     Remove String       ${get_text}     ${SPACE}
    ${isTrue}   Run Keyword And Return Status    Should Contain Any     ${remove_space}     ${java}
    Run Keyword If    "${isTrue}"=="True"     Set Test Message   ${remove_space} is displayed\n        append=yes
    ...     ELSE    Log To Console  ${isTrue}

Verify if Python Tool is displayed
    Wait Until Keyword Succeeds     10         2000ms       Element Should Be Visible   ${LBL_PYTHON}
    ${get_text}     Get Text    ${LBL_PYTHON}
    Log To Console  ${get_text}
    ${remove_space}     Remove String       ${get_text}     ${SPACE}
    ${isTrue}   Run Keyword And Return Status    Should Contain Any     ${remove_space}     ${python}
    Run Keyword If    "${isTrue}"=="True"     Set Test Message   ${remove_space} is displayed\n        append=yes
    ...     ELSE    Log To Console  ${isTrue}

Verify if RF Tool is displayed
    Wait Until Keyword Succeeds     10         2000ms       Element Should Be Visible   ${LBL_RF}
    ${get_text}     Get Text    ${LBL_RF}
    Log To Console  ${get_text}
    ${remove_space}     Remove String       ${get_text}     ${SPACE}
    ${isTrue}   Run Keyword And Return Status    Should Contain Any     ${remove_space}     ${rf}
    Run Keyword If    "${isTrue}"=="True"     Set Test Message   ${remove_space} is displayed\n        append=yes
    ...     ELSE    Log To Console  ${isTrue}

Verify if Career Objective is displayed
    Wait Until Keyword Succeeds     10         2000ms       Element Should Be Visible   ${LBL_CAREER}
    ${get_text}     Get Text    ${LBL_CAREER}
    Log To Console  ${get_text}
    ${remove_space}     Remove String       ${get_text}     ${SPACE}
    ${isTrue}   Run Keyword And Return Status    Should Contain Any     ${remove_space}     ${career}
    Run Keyword If    "${isTrue}"=="True"     Set Test Message   ${remove_space} is displayed\n        append=yes
    ...     ELSE    Log To Console  ${isTrue}

Click Validate and Send
    Wait Until Keyword Succeeds     10         2000ms       Click Element   ${BTN_VALIDATE}

Succes Message
    ${isVisible}    Run Keyword And Return Status    Wait Until Keyword Succeeds     10         2000ms       Element Should Be Visible   ${LBL_SUCCESS}
    ${get_message}      Get Text    ${LBL_SUCCESS}
    ${isVisibles}    Run Keyword And Return Status       Should Contain    ${get_message}   Thank you for submitting your project
    Log To Console      ${isVisibles}
    Run Keyword If  "${isVisible}"=="True"      Set Test Message   PASSED
    ...     ELSE   Set Test Message  FAILED to SUBMIT
